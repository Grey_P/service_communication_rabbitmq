﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IPlayerService
    {
        void ListenQueue();
        void SendMessageToQueue(string message);
    }
}
