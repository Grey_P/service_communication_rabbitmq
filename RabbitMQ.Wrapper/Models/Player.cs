﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;
using RabbitMQ.Wrapper.Services;

namespace RabbitMQ.Wrapper.Models
{
    public class Player
    {
        public ConnectionFactory factory { get; set; }
        public IConnection connection { get; set; }
        public IModel channel { get; set; }

        public string OwnQueueName { get; set; }
        public string OpponentQueueName { get; set; }
        public string answerText { get; set; }
    }
}
