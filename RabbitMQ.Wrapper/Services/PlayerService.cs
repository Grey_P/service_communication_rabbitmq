﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.GlobalSettings;

namespace RabbitMQ.Wrapper.Services
{
    public class PlayerService: IDisposable, IPlayerService
    {
       Player player;
       public PlayerService(string OwnQueue, string OpponentOueue, string answerText)
        {
            player = new Player();
          
            this.player.answerText = answerText;
            this.player.OwnQueueName = OwnQueue;
            this.player.OpponentQueueName = OpponentOueue;

            player.factory = new ConnectionFactory() { HostName = Settings.url };

            player.connection = player.factory.CreateConnection();
            player.channel = player.connection.CreateModel();
            player.channel.BasicQos(0, 1, false);

            player.channel.QueueDeclare(queue: player.OwnQueueName,
                 durable: true,
                 exclusive: false,
                 autoDelete: false,
                 arguments: null);
            player.channel.QueueDeclare(queue: player.OpponentQueueName,
                 durable: true,
                 exclusive: false,
                 autoDelete: false,
                 arguments: null);
            var properties = player.channel.CreateBasicProperties();
            properties.Persistent = true;
            ListenQueue();
        }

        public void ListenQueue()
        {
            var consumer = new EventingBasicConsumer(player.channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(DateTime.Now + ":" + message);
                Thread.Sleep(2500);
                SendMessageToQueue(this.player.answerText);
            };
            player.channel.BasicConsume(queue: player.OwnQueueName,
                                 autoAck: true,
                                 consumer: consumer);
        }

        public void SendMessageToQueue(string message)
        {
            if (message != ""){
                var body = Encoding.UTF8.GetBytes(message);
                player.channel.BasicPublish(exchange: "",
                                  routingKey: player.OpponentQueueName,
                                  basicProperties: null,
                                  body: body);
            }
        }

        public void Dispose()
        {
            player.connection?.Dispose();
            player.channel?.Dispose();
        }
    }
}
