﻿using System;
using RabbitMQ.Wrapper.Services;

namespace Pinger
{
    class Program
    {
        // Start: dotnet run Ping
        static void Main(string[] args)
        {
            try
            {
                PlayerService pp = new PlayerService("ping_queue", "pong_queue", "Ping");
                pp.SendMessageToQueue(GetMessage(args));
                Console.ReadLine();
            } catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
        private static string GetMessage(string[] args)
        {
            if (args.Length > 0)
            {
                return string.Join(" ", args);
            }
            else return "";
        }
    }
}
